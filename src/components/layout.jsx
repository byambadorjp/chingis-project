
import React, { useContext } from 'react';
import { ProgressLogin } from '../components';
import { Navigation } from '../pages/navigation';
import { useLocation } from 'react-router-dom';
import _ from 'lodash';
import { SignUpContext } from '../providers/sign-up-provider.jsx';

export const Layout = ({ children, color }) => {
    const location = useLocation();
    const { step } = useContext(SignUpContext);

    return (
        <div>
            {
                !_.isEmpty(
                    _.chain([
                        '/register',
                    ])
                        .filter(path => location.pathname.match(path))
                        .value()
                )
                && <ProgressLogin step={step} />
            }
            {
                color === true ?
                    <div className='containerpf b-gray6 font-main'>
                        <Navigation />
                        {children}
                    </div>
                : 
                <div className='containerpf font-main'>
                    <Navigation />
                    {children}
                </div>
            }
        </div>
    )
}