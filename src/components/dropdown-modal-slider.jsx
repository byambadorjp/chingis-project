import React from 'react';

export const DDModalSlider = ({ pop, closeSlider, className, children}) => {
    return (
        <>
            {pop && <div className="backdrop" onClick={closeSlider}></div>}
            <div className={`slider flex-center bshadow bradius-5 ${pop && 'slider-animation'}`}>
                {children}
            </div>
        </>
    );
};