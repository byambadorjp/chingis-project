import React from 'react';
import { FbIcon, GmailIcon } from './icons';

export const Button = (props) => {
    let { onClick, children, disabled, icon = null, className, ...others } = props;

    if (icon === 'facebook') {
        return (
            <button onClick={onClick} className={`btn flex flex-row items-center pa-10 ${className} ${disabled && 'disabled'}`} {...others}>
                <div>
                    <FbIcon width={11} height={20} />
                </div>
                <div className="justify-center">
                    <div className="w-1 h-30 b-light-gray ml-18"></div>
                </div>
                    <div className="margin-auto">
                    {children}
                </div>
            </button>
        )
    }
    if (icon === 'gmail') {
        return (
            <button onClick={onClick} className={`btn flex flex-row items-center pa-10  ${className} ${disabled && 'disabled'}`} {...others}>
                <GmailIcon width={20} height={20} />
                
                    <div className="w-1 h-30 b-light-gray ml-10 mr-10"></div>
                {children}
               

            </button>
        )
    }

    if (icon === 'signupin') {
        return (
            <button onClick={onClick} className={`btn flex flex-row items-center justify-evenly ${className} ${disabled && 'disabled'}`} {...others}>
                {/* <div className="w-2 h-30 ml-10 mr-10"></div> */}
                {children}
            </button>
        )
    }

    return (
        <button onClick={onClick} className={` btn ${className} ${disabled && 'disabled'}`} {...others}>
            {children}
        </button>
    );
};