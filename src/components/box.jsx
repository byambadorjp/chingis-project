import React from 'react';

export const Box = (props) => {
    let { children, type = "default", disabled, className, comment = false, ...others } = props;

    const getBorderClass = () => type === "top" ? "box-top" : type === "bottom" ? "box-bottom" : "box";
   
    return (
        <>
            {
                comment ? 
                    <div className={`b-default ml-8 mr-8 ${getBorderClass()} ${className}`} {...others}>
                        <div className='container'>
                            {/* ADD A pv-${i}(PADDING TOP & BOTTOM) URSELF */}
                            {children}
                        </div>
                    </div>
                :
                    <div className={`b-default ${getBorderClass()} ${className}`} {...others}>
                        <div className='container'>
                            {/* ADD A pv-${i}(PADDING TOP & BOTTOM) URSELF */}
                            {children}
                        </div>
                    </div>
            }
        </>
    );
};