import React, { useState, useRef, useContext } from 'react'
import { Stack, MenuIcon, ProfileIcon, NewProjectIcon, XIcon, HomeIcon, LogOut, LogOutDropdown, DDModalSlider } from '../components'
import { DownArrow } from './icons';
import { useHistory } from 'react-router-dom'
import { useOutsideClick } from '../Hooks/use-outside-click'
import { useDoc } from '../Hooks/firebase'
import { AuthContext } from '../providers/auth-user-provider'

export const DropdownItem = ({ children, onClick, ...others }) => {

    return (
        <div className='flex-row items-center bradius-10 h25' onClick={onClick} {...others}>
            {children}
        </div>
    )
}

export const DropdownItemArrow = ({ children, onClick, ...others }) => {
    return (
        <div className='flex-center' onClick={onClick} {...others}>
            {children}
        </div>
    )
}

export const DropdownOptions = ({ children }) => {
    return (
        <div className='absolute b-default pa-vw-5 w-vw-80 l-0 margin-auto r-0 bshadow h-200 z-i bradius-10' size={4}>
            {children}
        </div>
    )
}

export const DropdownOptionsArrow = ({ children }) => {
    return (
        <Stack className='text-center font-DmSans b-default br-inactive-1 bradius-10 flex-center' size={2}>
            {children}
        </Stack>
    )
}

export const DropdownMenu = () => {
    const history = useHistory();
    const [show, setShow] = useState(false);
    // const [signedIn, setSignedIn] = useState(false);
    const [pop, setPop] = useState(false)
    const ref = useRef();
    const { user } = useContext(AuthContext)
    const { uid } = user || {};
    const { data: userData } = useDoc(`users/${uid}`);

    const [clicked, setClicked] = useState(false);
    console.log(clicked)
    const sheesh = () => {
        setClicked((clicked) => {
            return !clicked
        })
    }

    useOutsideClick(ref, () => { setShow(false) });

    return (
        <div className='z-i bradius-10'>
            <div className='container'>
                {!show && <MenuIcon height={12} width={18} onClick={() => setShow(true)} color={'#2F80ED'} className="mr-15 c-fb-color" />
                }
                {show && userData && userData.id === uid &&
                    <DropdownOptions>
                        <XIcon className='flex justify-end' height={15} width={15} onClick={() => setShow(false)} />
                        <div className="flex-col justify-between flex" style={{ height: '90%' }}>
                            <DropdownItem onClick={() => { sheesh(); history.push('/addEvent') }}>
                                <NewProjectIcon height={20} width={20} />
                                <div className='ml-20 fs-18 font-main mb-4'>Шинэ аян үүсгэх</div>
                            </DropdownItem>
                            <DropdownItem onClick={() => { sheesh(); history.push('/profile') }}>
                                <ProfileIcon height={20} width={20} />
                                <div className='ml-20 fs-18 font-main mb-4'>Хэрэглэгч</div>
                            </DropdownItem>
                            <DropdownItem onClick={() => { history.push('/feed') }}>
                                <HomeIcon height={20} width={20} />
                                <div className='ml-20 fs-18 font-main mb-4'>Нүүр хуудас</div>
                            </DropdownItem>
                            <DropdownItem onClick={() => { sheesh(); setPop(true) }} ref={ref}>
                                <LogOut height={20} width={20} /><div className='ml-20 fs-18 font-main c-warning mb-4'>Log Out</div>
                                {pop &&
                                    <DDModalSlider pop={pop} closeSlider={(pop) => { setPop(!pop) }}>
                                        <LogOutDropdown />
                                    </DDModalSlider>
                                }
                            </DropdownItem>
                        </div>
                    </DropdownOptions>}
                {show && userData && userData.id !== uid &&
                    <DropdownOptions>
                        <XIcon className='flex justify-end' height={15} width={15} onClick={() => setShow(false)} />
                        <div className='flex-col items-center'>
                            <div className='flex-center'>
                                <div className='ml-20 fs-18 lh-24 font-main text-center mt-50'>Та ийшээ орхын тулд бүртгүүлэх хэрэгтэй.</div>
                            </div>
                            <div className='btm flex-center fs-18 lh-22 bold font-main primary-gradient c-white w100 h-50 bradius-bottom-right bradius-bottom-left' onClick={() => { history.push('/register') }}>Бүртгүүлэх</div>
                        </div>
                    </DropdownOptions>
                }
            </div>
        </div>
    )
}

export const DropdownMenuArrow = ({ type, setType }) => {
    const [show, setShow] = useState(false);
    const ref = useRef()

    useOutsideClick(ref, () => { setShow(false) });

    const [clicked, setClicked] = useState(false);

    const isItClicked = () => {
        setClicked((clicked) => {
            return !clicked
        })
    }

    return (
        <div className='pr font-main w-150 display-block mr-15' ref={ref}>
            {!show && <div className="fs-16 lh-21 text-right mt-12 flex bold justify-end items-center w100" onClick={() => { setShow((show) => !show) }}>
                {type}
                <DownArrow className="ml-10 mt-6 mr-5" height={18} width={12} />
            </div>}
            {show &&
                <div className='pr font-main w-150 display-block' ref={ref}>
                    <div className="fs-16 lh-21 text-right mt-12 flex bold justify-end items-center w100" onClick={() => { setShow((show) => !show) }}>
                        <div className='flex items-center'>
                            <div className='fs-16 lh-21 text-right display-block justify-end items-center'>{type}</div>
                            <DownArrow className="ml-10 mt-6 mr-5" height={18} width={12} />
                        </div>
                    </div>
                    <div className='absolute w100'>
                        <DropdownOptionsArrow>
                            <DropdownItemArrow onClick={() => { setType('Шинэ'); isItClicked() }} className={`${clicked ? 'c-dark' : 'srt-by-bg-color c-fb-color w90 bradius-5 margin-top-8'}`}>
                                <div className={`${clicked ? 'mt-16 mb-10 bold' : 'mt-8 mb-8 bold'}`}>Шинэ</div>
                            </DropdownItemArrow>
                            <div className="b-inactive w100 h-1" />
                            <DropdownItemArrow onClick={() => { setType('Хандалт ихтэй'); isItClicked() }} className={`${clicked ? 'c-fb-color srt-by-bg-color w90 bradius-5 margin-btm-8' : 'c-dark'}`}>
                                <div className={`${clicked ? 'mb-8 mt-8 bold' : 'mb-16 mt-10 bold'}`}>Хандалт ихтэй</div>
                            </DropdownItemArrow>
                        </DropdownOptionsArrow>
                    </div>
                </div>}
        </div>
    )
}