import React from 'react';
// import {PenIcon} from '../components/icons/pen-icon'

export const Input = (props) => {
    let { className, inputRef, usage, ...others } = props;

    return (
        <div>
            <input ref={inputRef} className={`input b-blue-dark ${className}`} {...others}/>
        </div>
    );
};