import React, { useState } from 'react';

export const TenderCampaign2 = ({ toggle }) => {
    const [clicked, setClicked] = useState(false)

    const toggleAnimation = () => {
        if (clicked === false) {
            setClicked(true)
            toggle()
        } else {
            setClicked(false)
            toggle()
        }
    }

    return (
        <div className="flex-col mt-20 h-36 bradius-10">
            <div className="toggle-menu pr w90 b-white flex-row h-36">
                <div className={`flex-center w50 font-main text-center mt-5 bradius-top-left-10 bradius-top-right-10 ${clicked ? 'c-gray bb-gray6-1' : 'c-secondary bold bb-secondary-2'}`} onClick={toggleAnimation}>Verified</div>
                <div className={`flex-center w50 font-main text-center mt-5 bradius-top-right-10 bradius-top-left-10 ${clicked ? 'c-secondary bold bb-secondary-2' : 'c-gray bb-gray6-1'}`} onClick={toggleAnimation}>Not Verified</div>
                <div className={`b-secondary h-36 menu-indicator op-little bradius-10 ${clicked ? 'active' : ''}`} />
            </div>
        </div>
    )
}