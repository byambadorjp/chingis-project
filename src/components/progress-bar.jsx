import React from 'react';

export const ProgressBar = ({ vote, voteCount }) => {
    return (
        <div className='pr mt-3'>
            <div className='flex justify-center'>
                <div className='w90 absolute bradius-10 b-gray4 h-10 mt-7' />
            </div>
            <div className='absolute bradius-50 third-gradient h-10 ml-17 progress-bar-max-width' style={{ width: `${(vote / voteCount) * 100 || 1}%` }} />
        </div>
    )
}

export const ProgressLogin = ({step}) => {
    return (
        <div className='pr z-5'>
            <div className='flex justify-center'>
                <div className='w100 absolute b-gray4 h-5' />
            </div>
            <div className='absolute progress-login b-primary h-5' style={{ width: `${(step / 4) * 100 || 1}%` }} />
        </div>
    )
}