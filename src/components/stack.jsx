import React from 'react';

export const Stack = ({ className, children, size = 1, horizontal }) => {
    const baseSpace = 4;
    let style = {
        container: {
            display: 'grid',
            gridTemplateColumns: `100%`,
            gridGap: `${baseSpace * size}px`,
            backgroundColor: 'gray2'
        },
        horizontal: {
            display: 'inline-grid',
            gridGap: `${baseSpace * size}px`,
            gridTemplateColumns: `repeat(${children}, auto)`,
        },
    };
    return (
        <div className={className} style={(horizontal ? style.horizontal : style.container)}>
            {children}
        </div>
    );
};