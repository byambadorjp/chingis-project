import React from 'react'

export const SearchIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M17.5358 17.5357L13.6519 13.6517M15.7501 8.60709C15.7501 12.552 12.5521 15.7499 8.60721 15.7499C4.66232 15.7499 1.46436 12.552 1.46436 8.60709C1.46436 4.6622 4.66232 1.46423 8.60721 1.46423C12.5521 1.46423 15.7501 4.6622 15.7501 8.60709Z" stroke="#99A3BE" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}
