import React from 'react'

export const BlueMenuIcon = ({ height, width, color = "#FEFEFE", ...others }) => {
    return (
        <span {...others}>
            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5 14H23M5 8H16.5M11.5 20H23" stroke="#2F80ED" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>

    )
}