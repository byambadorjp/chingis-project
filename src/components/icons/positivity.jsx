import React from 'react'

export const positivityIcon = ({ height, width, color = "#2F80ED", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 172 112" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g opacity="0.2">
                <path d="M18.9143 1V6M18.9143 11V6M18.9143 6H23.7143H13.7143" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M18.9143 21V26M18.9143 31V26M18.9143 26H23.7143H13.7143" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M18.9143 41V46M18.9143 51V46M18.9143 46H23.7143H13.7143" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M43.6286 1V6M43.6286 11V6M43.6286 6H48.4286H38.4286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M43.6286 21V26M43.6286 31V26M43.6286 26H48.4286H38.4286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M43.6286 41V46M43.6286 51V46M43.6286 46H48.4286H38.4286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M68.3429 1V6M68.3429 11V6M68.3429 6H73.1429H63.1429" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M68.3429 21V26M68.3429 31V26M68.3429 26H73.1429H63.1429" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M68.3429 41V46M68.3429 51V46M68.3429 46H73.1429H63.1429" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M93.0572 1V6M93.0572 11V6M93.0572 6H97.8572H87.8572" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M93.0572 21V26M93.0572 31V26M93.0572 26H97.8572H87.8572" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M93.0572 41V46M93.0572 51V46M93.0572 46H97.8572H87.8572" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M117.771 1V6M117.771 11V6M117.771 6H122.571H112.571" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M117.771 21V26M117.771 31V26M117.771 26H122.571H112.571" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M117.771 41V46M117.771 51V46M117.771 46H122.571H112.571" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M142.486 1V6M142.486 11V6M142.486 6H147.286H137.286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M142.486 21V26M142.486 31V26M142.486 26H147.286H137.286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M142.486 41V46M142.486 51V46M142.486 46H147.286H137.286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M167.2 1V6M167.2 11V6M167.2 6H172H162" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M167.2 21V26M167.2 31V26M167.2 26H172H162" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M167.2 41V46M167.2 51V46M167.2 46H172H162" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M18.9143 61V66M18.9143 71V66M18.9143 66H23.7143H13.7143" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M18.9143 81V86M18.9143 91V86M18.9143 86H23.7143H13.7143" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M18.9143 101V106M18.9143 111V106M18.9143 106H23.7143H13.7143" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M43.6286 61V66M43.6286 71V66M43.6286 66H48.4286H38.4286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M43.6286 81V86M43.6286 91V86M43.6286 86H48.4286H38.4286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M43.6286 101V106M43.6286 111V106M43.6286 106H48.4286H38.4286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M68.3429 61V66M68.3429 71V66M68.3429 66H73.1429H63.1429" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M68.3429 81V86M68.3429 91V86M68.3429 86H73.1429H63.1429" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M68.3429 101V106M68.3429 111V106M68.3429 106H73.1429H63.1429" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M93.0572 61V66M93.0572 71V66M93.0572 66H97.8572H87.8572" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M93.0572 81V86M93.0572 91V86M93.0572 86H97.8572H87.8572" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M93.0572 101V106M93.0572 111V106M93.0572 106H97.8572H87.8572" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M117.771 61V66M117.771 71V66M117.771 66H122.571H112.571" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M117.771 81V86M117.771 91V86M117.771 86H122.571H112.571" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M117.771 101V106M117.771 111V106M117.771 106H122.571H112.571" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M142.486 61V66M142.486 71V66M142.486 66H147.286H137.286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M142.486 81V86M142.486 91V86M142.486 86H147.286H137.286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M142.486 101V106M142.486 111V106M142.486 106H147.286H137.286" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M167.2 61V66M167.2 71V66M167.2 66H172H162" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M167.2 81V86M167.2 91V86M167.2 86H172H162" stroke="#2F80ED" strokeLinecap="round"/>
                <path d="M167.2 101V106M167.2 111V106M167.2 106H172H162" stroke="#2F80ED" strokeLinecap="round"/>
                </g>
            </svg>
        </span>
    )
}
