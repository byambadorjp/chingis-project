import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { useFirebase } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider'
import { FormInput, ActionButton, Stack, IndividualIcon, GroupIcon, ArrowIcon } from '../components'
import { SignUpContext } from '../providers/sign-up-provider.jsx';
import { SignUpNavbar } from './navigation'


const StepZero = () => {

    const { changeUserTypeInd, changeUserTypeGr, nextStep } = useContext(SignUpContext);

    return (

        <>  
            <div className="">
                <div className="h-vh-10 w-vw-90 margin-auto flex items-center justify-end c-blue">Алгасах</div>
                <div className="mt-50">
                    <div className=" font-main bold fs-24">Hii! </div>
                    <div className="c-gray3 font-Raleway fs-16 normal mt-24">Та төрлөө сонгоно уу ?</div>
                </div>

                <div className="flex justify-center fs-16">
                    <div className="c-white" onClick={changeUserTypeInd, nextStep}>Хувь хүн
                    <IndividualIcon />
                    </div>
                    <div className="c-white" onClick={changeUserTypeGr, nextStep}>Компани
                    <GroupIcon />
                    </div>
                </div>
            </div>

        </>
    )
}



const StepOne = () => {
    const { nextStep, error, setError } = useContext(SignUpContext);
    const { firebase, auth, firestore } = useFirebase();
    const history = useHistory();

    const signInWithGoogle = () => {
        var provider = new firebase.auth.GoogleAuthProvider();

        auth.signInWithPopup(provider).then((result) => {
            // var token = result.credential.accessToken;
            var user = result.user;

            firestore.collection('users').doc(user.uid).set({
                username: user.displayName,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                logged: "Google",
                profileImage: "default",
                uid: user.uid
            });

            if (user.emailVerified === true) {
                history.push('/feed')
            } else {
                history.push('/verify')
            }
        }).catch((error) => setError(error));
    }

    return (
        <>
            <SignUpNavbar />
            <Stack size={4} className="w-vw-90 margin-auto">
                <ActionButton className="font-main ws100 mt-6 h-50 c-primaryfourth brad-5 pa-12" icon='google' onClick={signInWithGoogle}>Google хаягаар бүртгүүлэх</ActionButton>
                <ActionButton className="font-main h-50 c-blue b-white brad-5 boxshadow" icon="white" onClick={nextStep}>Емайл хаягаар бүртгүүлэх</ActionButton>
                {error && <div className="c-red fs-14">{error}</div>}
                <div className="flex justify-center">
                    <div className="font-main c-dark">Хэдийн бүртгүүлсэн?</div>
                    <div className="ml-5 text-button c-blue bold" onClick={() => history.push('/login')}>Нэвтрэх</div>
                </div>
            </Stack>
        </>
    )
}


const StepTwo = () => {
    const { userType, error, usernameBind, emailBind, passwordBind, password2Bind, stepTwoCompleted, signUpPerson, signUpCompany } = useContext(SignUpContext);
    const history = useHistory()

    return (
        <div className="w-vw-90 h-vh-100 margin-auto">
            <div className="">
                <div className="w-vw-90 margin-auto h-vh-10 flex items-center justify-between">
                    <ArrowIcon width={15} height={15} onClick={() => { history.goBack() }} />
                    <div className="c-blue font-main" onClick={() => history.push('/feed')}>Алгасах</div>
                </div>
            </div>
            <div className="mb-48">
                <div className="font-main bold fs-24">Бүртгүүлэх </div>
                <div className="c-dark font-Raleway fs-16 normal mt-24">Та өөрийн бүртгэлээ үүсгэнэ үү? </div>
            </div>
            <Stack size={4}>
                <FormInput className="bradius-5 ph-44 h-50 br-border-dark" label='Хэрэглэгчийн нэр' type='text' icon='user' placeholder='Username' {...usernameBind} />
                <FormInput className="bradius-5 ph-44 h-50 br-border-dark" label='Цахим хаяг' placeholder='Name@mail.com' type='email' {...emailBind} />
                <FormInput className="bradius-5 ph-44 h-50 br-border-dark" label='Нууц үг' placeholder='Password' type='password' {...passwordBind} />
                <FormInput className="bradius-5 ph-44 h-50 br-border-dark" label='Нууц үгээ давтна уу?' placeholder='Password' type='password' {...password2Bind} />

                {error && <div className="c-red fs-14">{error}</div>}

                <ActionButton className="font-main h-50 c-primaryfourth b-primary brad-5" disabled={stepTwoCompleted()} icon="signupin" onClick={userType !== 'Group' ? signUpPerson : signUpCompany}>Бүртгүүлэх</ActionButton>


                <div className="flex justify-center">
                    <div className="font-main c-dark">Хэдийн бүртгүүлсэн?</div>
                    <div className="ml-5 text-button c-blue bold" onClick={() => history.push('/login')}>Нэвтрэх</div>
                </div>
            </Stack>
        </div>
    )
}


export const SignUp = () => {
    const { numberBind, numberFilled, addNumber } = useContext(SignUpContext);
    const { user } = useContext(AuthContext)
    const { step } = useContext(SignUpContext)
    const history = useHistory();

    if ((step === 0 || step === 4) && user) {
        history.push('/feed')
    }

    return (
        // <Layout>
        <div className="font-main text-center font-Raleway">
            <div className="flex-col" style={{ height: 'calc(100vh - 100px)' }}>
                {step === 0 && <StepZero />}
                {step === 1 && <StepOne />}
                {step === 2 && <StepTwo />}

                {step === 3 &&
                    <>
                        <div className="mb-150">
                            <div className="font-main bold fs-24">Утасны Дугаар </div>
                            <div className="c-gray3 font-Raleway fs-16 normal mt-24">Та утасны дугаараа оруулна уу.</div>
                        </div>

                        <Stack size={4}>
                            {/* havin a little error here */}
                            <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Хэрэглэгчийн нэр' type='text' icon='user' placeholder='Утасны дугаар' {...numberBind} />
                            <ActionButton className="font-main h-50 c-primaryfourth b-primary brad-5" disabled={numberFilled()} icon="signupin" onClick={addNumber}>Үргэлжлүүлэх</ActionButton>
                        </Stack>
                    </>
                }
            </div>
        </div>
        // </Layout>
    )
}
