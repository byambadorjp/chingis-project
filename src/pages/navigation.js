import React from 'react'
import { DropdownMenu, ArrowIcon, Grid } from '../components/';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import { PeopleWithText, WhiteArrowIcon } from '../components'

export const Navigation = ({ children }) => {

    let history = useHistory();

    return (
        <>
            {
                (history.location.pathname === '/login' || history.location.pathname === '/register') ?
                    <div></div>
                    :
                    <Grid className='h-70 shadow3 primary-gradient w100' columns="3">
                        <div className='flex items-center justify-start'>
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/feed',
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <div className="font-main ml-10 fs-24 lh-43 c-white" onClick={() => { history.push("/feed") }}>Dul</div>
                            }

                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/event-id',
                                        '/search',
                                        '/tender-id',
                                        "/verify-account",
                                        "/profile",
                                        '/vote-history',
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <ArrowIcon className="ml-20" onClick={() => { history.goBack() }} width={20} height={20} />
                            }

                        </div>
                        <div className='flex items-center justify-center'>
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/profile',
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <div className='items-center flex ml-10'>
                                    <span className='font-main bold fs-20'>Profile</span>
                                </div>
                            }
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/vote-history',
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <div className='items-center flex'>
                                    <span className='font-main bold fs-20'>History</span>
                                </div>
                            }
                        </div>
                        <div className='flex items-center justify-end'>
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/feed',
                                        '/event-id',
                                        '/search',
                                        '/tender-id',
                                        '/profile'
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <DropdownMenu ></DropdownMenu>
                            }

                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/register',
                                        '/login'
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <div className="c-white font-main mr-20" onClick={() => history.push('/feed')}>Алгасах</div>
                            }

                        </div>
                    </Grid>
            }
        </>
    )
}


export const SignUpNavbar = () => {
    const history = useHistory();

    return (
        <div className="primary-gradient w-vw-100 mb-48 h-vh-60">
            <div className="w-vw-90 margin-auto h-vh-10 flex items-center justify-between">
                <WhiteArrowIcon width={20} height={20} onClick={() => { history.goBack() }} />
                <div className="c-white font-main" onClick={() => history.push('/feed')}>Алгасах</div>
            </div>
            {
                history.location.pathname === '/register'
                    ?
                    <div className="mb-39">
                        <div className="font-main c-white bold fs-24">Бүртгүүлэх</div>
                        <div className="c-white font-Raleway fs-16 normal mt-24">Та өөрийн бүртгэлээ үүсгэнэ үү?</div>
                    </div>
                    :
                    <div className="mb-39">
                        <div className="font-main c-white bold fs-24">Нэвтрэх</div>
                        <div className="c-white font-Raleway fs-16 normal mt-24">Та өөрийн бүртгэлээр орно уу?</div>
                    </div>
            }
            <PeopleWithText />
        </div>
    )
}