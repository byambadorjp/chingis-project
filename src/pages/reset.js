import React, { useState, useEffect } from 'react'
import { ActionButton, FormInput, Layout } from '../components';
import { useFirebase } from '../Hooks';
import { useLocation } from 'react-router-dom'
import { queryString } from 'query-string'

export const Reset = () => {
    const [pass, setPass] = useState('');
    const [pass1, setPass1] = useState('');
    const [error, setError] = useState('');
    const { auth } = useFirebase();
    const [verify, setVerify] = useState('')
    // const history = useHistory()
    const incorrect = () => {
        //<Modal> here
    };
    const location = useLocation()
    const parsed = queryString.parse(location.search)
    let oobCode = parsed.oobCode
    // let oobCode = history.location.search.split('&');
    // oobCode = oobCode[1].split('oobCode=')[1]
    console.log(oobCode)
    // console.log(history)
    // const update = () => {
    //     auth.confirmPasswordReset(oobCode, pass ).then(() => {
    //         // Email sent.
    //         console.log("Updated")

    //     }).catch(function (error) {
    //         console.log(error)
    //     });
    // }

    useEffect(() => {
        console.log(auth)
        if (auth && oobCode) {
            console.log('UTGATAI BOL')
            auth.verifyPasswordResetCode(oobCode)
                .then((email) => {
                    setVerify('verified')
                    console.log(email)
                })
                .catch((error) => {
                    setError(error)
                })
        }
    }, [auth, oobCode])


    console.log(verify)

    return (
        <Layout>
            <div className='flex justify-center'>
                {
                    (verify === 'verified') &&
                    <div className='h-vh-70 justify-center'>
                        <h1 className='fs-24 title pt-73 pb-73'>Нууц үг солих</h1>
                        <FormInput value={pass} type='password' onChange={(e) => setPass(e.target.value)} placeholder='Шинэ нууц үг' className='mb-16 pl-44'></FormInput>
                        <FormInput value={pass1} type='password' onChange={(e) => setPass1(e.target.value)} placeholder='Давтаж оруулна уу' className='mb-16 pl-44'></FormInput>
                        {
                            pass === pass1 ?
                                <ActionButton className='margin-auto'>Солих</ActionButton>
                                :
                                <ActionButton className='margin-auto' onClick={incorrect}>Солих</ActionButton>
                        }
                    </div>
                }
                <h1>{error}</h1>
            </div>
        </Layout>
    )
}