import React, { createContext, useState, useContext } from 'react';
import { useInput, useFirebase, useDoc } from '../Hooks';
import { AuthContext } from './auth-user-provider';

export const SignUpContext = createContext({
    step: 0,
    error: '',
    userType: 'Individual',
    username: '',
    email: '',
    password: '',
    password2: '',
    number: '',
    prevStep: () => { },
    nextStep: () => { },
    setError: () => { },
    usernameBind: {},
    emailBind: {},
    passwordBind: {},
    password2Bind: {},
    numberBind: {},
    stepTwoCompleted: () => { }
})

export const SignUpProvider = ({ children }) => {
    const { firebase, auth, firestore } = useFirebase();
    const { user } = useContext(AuthContext);
    const { uid } = user || {};
    console.log(uid);
    const { updateRecord: updateProfile } = useDoc(`users/${uid}`);
    let [step, setStep] = useState(0)
    let [error, setError] = useState(null);
    let [userType, setUserType] = useState('');

    const [username, usernameBind] = useInput('');
    const [email, emailBind] = useInput('');
    const [password, passwordBind] = useInput('');
    const [password2, password2Bind] = useInput('');
    const [number, numberBind] = useInput('');


    const prevStep = () => { setStep((step) => step <= 0 ? step : step - 1) }
    const nextStep = () => { setStep((step) => step > 3 ? step : step + 1) }

    const stepTwoCompleted = () => {
        if (username === '') return true;
        if (email === '') return true;
        if (password === '') return true;
        if (password2 === '') return true;
        return false
    }

    const numberFilled = () => {
        if (number === '') return true;
        return false
    }

    const addNumber = async () => {
        console.log('clicked!')
        if (!(number)) {
            setError('Please enter your mobile number');
            return;
        }

        await updateProfile({
            phone: number
        })
        nextStep();
    }


    const changeUserTypeInd = () => {
        setUserType("Individual")
    }

    const changeUserTypeGr = () => {

        setUserType("Group")
    }

    const signUpPerson = async () => {
        if (password !== password2) {
            setError('passwords dont match');
            return;
        }
        let cred = await auth.createUserWithEmailAndPassword(email, password)
            .catch((error) => {
                setError(error.message)
            })

        if (cred) {
            await firestore.collection('users').doc(cred.user.uid).set({
                username: username,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                logged: "default",
                profileImage: "default",
                number: number,
                uid: cred.user.uid
            });
            nextStep();
        }
    }

    const signUpCompany = async () => {
        if (!(username && email)) {
            setError('Please enter all required information');
            return;
        }
        if (password !== password2) {
            setError('passwords dont match');
            return;
        }
        let group = await auth.createUserWithEmailAndPassword(email, password)
            .catch((error) => {
                alert(error.message)
            })

        await firestore.collection('users').doc(group.user.uid).set({
            username: username,
            createdAt: firebase.firestore.FieldValue.serverTimestamp(),
            logged: "Group",
            profileImage: "default",
            uid: group.user.uid
        });

        nextStep();
    }

    return (
        <SignUpContext.Provider value={{
            step,
            error,
            prevStep,
            nextStep,
            setError,
            userType,
            setUserType,
            username,
            usernameBind,
            email,
            emailBind,
            password,
            passwordBind,
            password2,
            password2Bind,
            stepTwoCompleted,
            signUpPerson,
            signUpCompany,
            changeUserTypeInd,
            changeUserTypeGr,
            numberFilled,
            numberBind,
            addNumber
        }}>
            {children}
        </SignUpContext.Provider>
    )
}
